#[macro_use]
extern crate serde;
extern crate serde_derive;
use serde::Serialize;
use serde::Deserialize;
use wasm_bindgen::prelude::*;

#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[derive(Serialize)]
struct Discount {
    ruleId: String,
    discountType: String,
    name: String,
    applyNext: bool,
    //    incompatibleDiscounts: String, // return null
    amt: f32,
    qtyOffer: i32,
    calculatedOnDiscountEngine: bool,
    obdiscQtyoffer: i32,
    displayedTotalAmount: f32,
    fullAmt: f32,
    actualAmt: f32,
    unitDiscount: f32,
    baseGrossUnitPrice: f32,
    baseNetUnitPrice: f32,
}

#[derive(Serialize)]
struct DiscountLine {
    id: String,
    grossUnitAmount: f32,
    grossUnitPrice: f32,
    discounts: Vec<Discount>,
}

#[derive(Serialize)]
struct DiscountResult {
    lines: Vec<DiscountLine>,
}

#[derive(Deserialize)]
struct Ticket {
    lineId : String,
    price : f32,
    qty: i32
}

#[wasm_bindgen]
extern "C" {
    pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn applyDiscount(ticket: &JsValue) -> JsValue{
    // alert(&format!("First part"));
    let ticketTransformed : Vec<Ticket> = JsValue::into_serde(ticket).unwrap();

    let mut discountLines: Vec<DiscountLine> = Vec::with_capacity(ticketTransformed.len());
    for ticketLine in ticketTransformed {
        // receives some things
        let mut discounts: Vec<Discount> = Vec::with_capacity(2);
        discounts.push(Discount {
            ruleId: String::from("C26B841C84B14FE2AB1A334DD3672E87"),
            discountType: String::from("697A7AB9FD9C4EE0A3E891D3D3CCA0A7"),
            name: String::from("RUST Discount 10 %"),
            applyNext: false,
            amt: ticketLine.price * ticketLine.qty as f32 * 0.1,
            qtyOffer: 1,
            calculatedOnDiscountEngine: true,
            obdiscQtyoffer: 1,
            displayedTotalAmount: ticketLine.price * ticketLine.qty as f32 * 0.9,
            fullAmt: ticketLine.price * ticketLine.qty as f32 * 0.1,
            actualAmt: ticketLine.price * ticketLine.qty as f32 * 0.1,
            unitDiscount: 1.5,
            baseGrossUnitPrice: ticketLine.price * 0.9,
            baseNetUnitPrice: ticketLine.price * 0.9,
        });
        discounts.push(Discount {
            ruleId: String::from("C26B841C84B14FE2AB1A334DD3672E87"),
            discountType: String::from("697A7AB9FD9C4EE0A3E891D3D3CCA0A7"),
            name: String::from("ANOTHER Discount 5 %"),
            applyNext: false,
            amt: ticketLine.price * ticketLine.qty as f32 * 0.05,
            qtyOffer: 1,
            calculatedOnDiscountEngine: true,
            obdiscQtyoffer: 1,
            displayedTotalAmount: ticketLine.price * ticketLine.qty as f32 * 0.9,
            fullAmt: ticketLine.price * ticketLine.qty as f32 * 0.05,
            actualAmt: ticketLine.price * ticketLine.qty as f32 * 0.05,
            unitDiscount: 1.5,
            baseGrossUnitPrice: ticketLine.price * 0.95,
            baseNetUnitPrice: ticketLine.price * 0.95,
        });
        let discountLine: DiscountLine = DiscountLine {
            id: ticketLine.lineId,
            grossUnitAmount: ticketLine.price * ticketLine.qty as f32 * 0.85,
            grossUnitPrice: ticketLine.price * 0.85,
            discounts: discounts,
        };
        discountLines.push(discountLine)
    }

    let discountResult = DiscountResult {
        lines: discountLines,
    };

    return JsValue::from_serde(&discountResult).unwrap();
    // alert(&format!("Hello, {}!", stringify(discountResult)));
}
